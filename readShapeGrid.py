import glob, os
import shapefile
import imageio, cv2
import numpy as np

def getGridXY(gridBbox, cellBbox):
	gridSideX = abs(gridBbox[0] - gridBbox[2])
	gridSideY = abs(gridBbox[1] - gridBbox[3])
	cellSideX = abs(cellBbox[0] - cellBbox[2])
	cellSideY = abs(cellBbox[1] - cellBbox[3])
	return int(gridSideX / cellSideX), int(gridSideY / cellSideY), cellSideX, cellSideY

def getCellXY(gridBbox, cellBbox, cellSideX, cellSideY):
	distanceFromGridBboxX = abs(gridBbox[0] - cellBbox[0])
	distanceFromGridBboxY = abs(gridBbox[1] - cellBbox[1])	
	return abs(int(distanceFromGridBboxX / float(cellSideX))), abs(int(distanceFromGridBboxY / float(cellSideY)))
	
# Read all files from directory. Sort ASC
os.chdir("TimeSeriesShp")
shpFileNames = []
for shpFileName in glob.glob("*.shp"):
	shpFileNames.append(shpFileName)
shpFileNames.sort(key=lambda f: int(filter(str.isdigit, f)))

# TODO: Pygame animation by normalizing to rgb red 0-255.

sir = []
infectedPrevious = []
init = True
recordCount = 0
timeStep = 0
xGrid = 154
yGrid = 99
cellSideX = 0
cellSideY = 0
scale = 4

maxInfected = 0
maxSusceptible = 0
maxRecovered = 0
maxBeta = 0

maxVariableValue = 255
defaultVariableProportion = 0.2

# Find max values for later normalization
for shpFileName in shpFileNames:
	sf = shapefile.Reader(shpFileName)
	print("Finding max: " + shpFileName + "...")
	records = sf.records()
	shapes = sf.shapes()
	recordCount = len(shapes)
	
	for i in range(recordCount):
		data = records[i]
		infected = data[3]
		susceptible = data[1]
		recovered = data[4]
		beta = data[5]
		if infected > maxInfected:
			maxInfected = infected
		if susceptible > maxSusceptible:
			maxSusceptible = susceptible
		if recovered > maxRecovered:
			maxRecovered = recovered
		if beta > maxBeta:
			maxBeta = beta

print("Max beta: " + str(maxBeta))

for shpFileName in shpFileNames:

	sf = shapefile.Reader(shpFileName)
	print("Rasterizing: " + shpFileName + "...")

	records = sf.records()
	shapes = sf.shapes()
	gridBbox = sf.bbox
	recordCount = len(shapes)

	if (init == True):
		infectedPrevious = [0] * recordCount

	for i in range(recordCount):
		bbox = shapes[i].bbox
		
		if (init == True):
			xGrid, yGrid, cellSideX, cellSideY = getGridXY(gridBbox, bbox)

			lambdaSir6 = np.zeros((yGrid, xGrid, 3), dtype=np.uint8)
			#lambdaSir6.fill(255) # Everything is white
			init = False

		xCell, yCell = getCellXY(gridBbox, bbox, cellSideX, cellSideY)

		data = records[i]
		infected = data[3]
		recovered = data[4]
		susceptible = data[1]
		beta = data[5]
	
		forceOfInfection = 0
		if susceptible > 0:
			forceOfInfection = (infected - infectedPrevious[i]) / (susceptible * 1) # lambda = new infected / (S * days) (see: https://en.wikipedia.org/wiki/Force_of_infection)
	
		infectedPrevious[i] = infected # update the infected for the next day
		polygonId = data[0]
		
		lambdaSir6[yGrid - yCell -1 , xCell, 2] = 255 - int((susceptible / float(maxSusceptible))*(255 * 0.75)) + 0.25 * 255
		lambdaSir6[yGrid - yCell -1 , xCell, 1] = 0
		lambdaSir6[yGrid - yCell -1 , xCell, 0] = 0

		#if recovered > 0:
			#lambdaSir6[yGrid - yCell -1 , xCell, 0] = 0
			#lambdaSir6[yGrid - yCell -1 , xCell, 1] = 255
			#lambdaSir6[yGrid - yCell -1 , xCell, 2] = 0
		if infected > 0:
			infectedRatio = infected / float(maxInfected)

			if infectedRatio <= 0.33: # green
				lambdaSir6[yGrid - yCell -1, xCell, 0] = 0
				lambdaSir6[yGrid - yCell -1, xCell, 1] = 255
				lambdaSir6[yGrid - yCell -1, xCell, 2] = 0			
			elif infectedRatio > 0.66: # red
				lambdaSir6[yGrid - yCell -1, xCell, 0] = 255
				lambdaSir6[yGrid - yCell -1, xCell, 1] = 0
				lambdaSir6[yGrid - yCell -1, xCell, 2] = 0
			else: # yellow
				lambdaSir6[yGrid - yCell -1, xCell, 0] = 255 # Red
				lambdaSir6[yGrid - yCell -1, xCell, 1] = 255
				lambdaSir6[yGrid - yCell -1, xCell, 2] = 0 # Reda

		#lambdaSir6[yGrid - yCell -1, xCell, 0] = int((beta / float(maxBeta)) * 255)
		#lambdaSir6[yGrid - yCell -1, xCell, 1] = 0
		#lambdaSir6[yGrid - yCell -1, xCell, 2] = 0

			#lambdaSir6[yGrid - yCell -1, xCell, 2] = 255 - int((infected / float(maxInfected))*(255 * 0.9)) + 0.1 * 255
	if timeStep < 100:
		if timeStep < 10:
			timeStepStr = "00" + str(timeStep)
		else:
			timeStepStr = "0" + str(timeStep)
	else:
		timeStepStr = str(timeStep)


	#imageio.imwrite("infected_timeStep" + timeStepStr + ".png", lambdaSir6)
	imageio.imwrite("infected_timeStep" + timeStepStr + ".png", cv2.resize(lambdaSir6, (xGrid * scale, yGrid * scale), interpolation=cv2.INTER_NEAREST))
	timeStep += 1
